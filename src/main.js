import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'

Vue.config.productionTip = false

axios.defaults.baseURL = 'https://www.googleapis.com/youtube/v3'

new Vue({
	router,
	render: h => h(App)
}).$mount('#app')
